It is an interactive multi-use live-work development that will deliver a versatile mix of micro retail, office, and residential space for lease or purchase.

The planned community includes public and private event spaces and will be built as a community destination for living, business and entertainment.

Website : http://thebloqparq.com